#setting your dependencies of gradle project
    classpath "com.google.dagger:hilt-android-gradle-plugin:2.35"
    classpath "androidx.navigation:navigation-safe-args-gradle-plugin:2.3.5"
#setting your plugins
    id 'kotlin-kapt'
    id 'dagger.hilt.android.plugin'
    id 'kotlin-parcelize'
    id 'androidx.navigation.safeargs.kotlin'

#Dependency used
#androidx arch component
    implementation "androidx.navigation:navigation-fragment-ktx:2.3.5"
    implementation "androidx.navigation:navigation-ui-ktx:2.3.5"
    implementation 'androidx.lifecycle:lifecycle-livedata-ktx:2.3.1'
    implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1'
    implementation "androidx.room:room-runtime:2.3.0"
    kapt "androidx.room:room-compiler:2.3.0"
    implementation "androidx.room:room-ktx:2.3.0"
#reactive
    implementation 'io.reactivex.rxjava3:rxjava:3.0.0'
    implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'
    implementation 'com.github.akarnokd:rxjava3-retrofit-adapter:3.0.0'
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.3"
#network
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.google.code.gson:gson:2.8.6"
    implementation "com.squareup.okhttp3:okhttp:4.9.0"
#DI
    implementation "com.google.dagger:hilt-android:2.35"
    kapt "com.google.dagger:hilt-compiler:2.35"
    implementation "androidx.activity:activity-ktx:1.2.3"
    implementation "org.koin:koin-android:2.2.0"
    implementation "org.koin:koin-android-scope:2.2.0"
    implementation "org.koin:koin-android-viewmodel:2.2.0"
#image stream
    implementation "com.github.bumptech.glide:glide:4.9.0"
    kapt "com.github.bumptech.glide:compiler:4.9.0"
#multidex
    implementation "androidx.multidex:multidex:2.0.1"
    
    --> set your defaultConfig : multiDexEnabled true

#local unit tests
    implementation 'androidx.test:core:1.3.0'
    testImplementation 'junit:junit:4.13.2'
    testImplementation 'org.hamcrest:hamcrest-all:1.3'
    testImplementation 'androidx.arch.core:core-testing:2.1.0'
    testImplementation 'org.robolectric:robolectric:4.5.1'
    testImplementation 'org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.3'
    testImplementation 'com.google.truth:truth:1.1.2'
    testImplementation 'org.mockito:mockito-core:2.23.0'
#instrumented unit tests
    androidTestImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.arch.core:core-testing:2.1.0'
    androidTestImplementation 'org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.3'
    androidTestImplementation 'com.google.truth:truth:1.1.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
    androidTestImplementation 'org.mockito:mockito-core:2.23.0'
